<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
		<a href="./">ホーム</a>
					<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<c:if test="${ not empty loginUser }">
				<div class="profile">
					<div class="name">
						<h2><c:out value="${loginUser.name}" /></h2>
					</div>
				</div>
			</c:if>

			<div class="form-area">
				<form action="newMessage" method="post">
					件名
					<textarea name="title" cols="60" rows="1" class="title"></textarea>
					（30文字まで）
					<br />
					カテゴリー
					<input name="category" size=20 class="category"></input>
					（10文字まで）
					<br />
					いま、どうしてる？<br />
					<textarea name="text" cols="100" rows="5" class="text"></textarea>
					<br />
					<input type="submit" value="投稿する">（1000文字まで）
				</form>
			</div>

			<div class="copylight"> Copyright(c)KishigawaMinami</div>
		</div>
	</body>
</html>