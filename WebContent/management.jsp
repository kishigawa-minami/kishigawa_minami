<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー管理</title>

	<script type="text/javascript">

	function disp(){
		if(window.confirm('本当にいいですか？')){
			location.href = "example_confirm.html";
		} else {
			window.alert('キャンセルされました');
			return false;
		}
	}

	</script>

</head>
<body>
	<div class="main-contents">
		<a href="./">ホーム</a>
		<a href="signup">ユーザー新規登録</a>
	</div>

	<table border = "3">
		<tr>
			<th>ID</th>
			<th>ログインID</th>
			<th>名前</th>
			<th>支店名</th>
			<th>所属・役職</th>
			<th>ユーザー</th>
			<th>登録日時</th>
			<th>更新日時</th>
			<th>編集</th>
		</tr>
		<c:forEach var="user" items="${userList}" >
			<tr>
				<td><c:out value="${user.id}" /></td>
				<td><c:out value="${user.login_id}" /></td>
				<td><c:out value="${user.name}" /></td>
				<td><c:out value="${user.branchName}" /></td>
				<td><c:out value="${user.positionName}" /></td>
				<td>
					<form action="stop" method="get">
						<c:if test="${user.stop == 0}">
							<input type="hidden" name="id" value="${user.id}" />
							<input type="submit" value="停止" onClick="return disp()"/>
						</c:if>
						<c:if test="${user.stop == 1}">
							<input type="hidden" name="id" value="${user.id}" />
							<input type="submit" value="復活"onClick="return disp()" />
						</c:if>
					</form>
				</td>
				<td><fmt:formatDate value="${user.createdDate}" pattern="yyyy/MM/dd HH:mm" /></td>
				<td><fmt:formatDate value="${user.updatedDate}" pattern="yyyy/MM/dd HH:mm" /></td>
				<td>
					<form action="useredit" method="get">
						<input type="hidden" name="id" value="${user.id}" />
						<input type="submit" value="編集" />
					</form>
				</td>
			</tr>
		</c:forEach>
	</table>
	<div class="copyright">Copyright(c)Kishigawa Minami</div>
</body>
</html>