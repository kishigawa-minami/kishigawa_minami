<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<!-- EL式を使用するか(falseで使用) -->
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板システム</title>
</head>
<body>
	<div class="main-contents">

		<c:if test="${ empty loginUser }">
			<a href="login">ログイン</a>
		</c:if>
		<c:if test="${ not empty loginUser }">
			<a href="./">ホーム</a>
			<a href="newMessage">新規投稿</a>
			<a href="management">ユーザー管理</a>
		</c:if>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>


		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
			</div>
		</c:if>

		<form action="./" method=get>
			<br> <label for="カテゴリー">カテゴリー<input type=text name="category" id="category" /></label>
			投稿開始日<input type="date" name="start">投稿終了日<input type="date" name="end"><input type="submit" value="検索">
		</form>

		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="account-name">
						<span class="login_id"><c:out value="${message.login_id}" /></span>
						<span class="name"><c:out value="${message.name}" /></span>
					</div>
					<div class="title">
						<c:out value="${message.title}" />
					</div>
					<div class="category">
						<c:out value="${message.category}" />
					</div>
					<div class="text">
						<c:out value="${message.text}" />
					</div>
					<div class="date">
						<fmt:formatDate value="${message.created_date}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
					<c:if test="${message.login_id == loginUser.login_id}">
						<form action="./" method="post">
							<input type="hidden" name="message.id" value="${message.id}" id="id">
							<input type="submit" value="削除">
						</form>
						</c:if>

					<div class="form-area">
						<c:forEach items="${comments}" var="comment">
							<c:if test="${message.id == comment.post_id }">
								<div class="text">
									<c:out value="${comment.text}" />
								</div>
								<c:if test="${comment.login_id == loginUser.login_id}">
								<form action="./" method="post">
									<input type="hidden" name="comment.id" value="${comment.id}" id="id">
									<input type="submit" value="削除">
								</form>
							</c:if>
							</c:if>
						</c:forEach>
						<form action="comment" method="post">
							コメント<br /> <input type="hidden" name="message_id"
								value="${message.id }">
							<textarea name="comment" cols="70" rows="5" class="comment"></textarea>
							<br /> <input type="submit" value="コメントする">（500文字まで）
						</form>
					</div>
				</div>
			</c:forEach>
		</div>
		<div class="copylight">Copyright(c)KishigawaMinami</div>
	</div>
</body>
</html>