<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="css/style.css" rel="stylesheet" type="text/css">
	<title>ユーザー新規登録</title>
	</head>
	<body>
		<div class="main-contents">
			<a href="./">ホーム</a>
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<form action="signup" method="post">
				<label for="name">名前</label> <input name="name" id="name" />（名前はあなたの公開プロフィールに表示されます）
				<label for="login_id">ログインID</label> <input name="login_id" id="login_id" />
				<label for="password">パスワード</label> <input name="password" type="password" id="password" />
				<label for="branch">支店名</label>
					<select name="branch">	<!-- 支店選択のプルダウン -->
					<option value="1">本社</option>
					<option value="2">支店A</option>
					<option value="3">支店B</option>
					<option value="4">支店C</option>
				</select>
				<br /> <label for="position">所属・役職名</label>		<!-- 所属・役職選択のプルダウン -->
				<select name="position">
					<option value="1">総務人事担当者</option>	<!-- 数値格納だから変える -->
					<option value="2">情報管理担当者</option>
					<option value="3">支店長</option>
					<option value="4">社員</option>
				</select>

				<br /><br /><input type="submit" value="登録" /> <br /> <a href="management">戻る</a>
				</form>
			<div class="copyright">Copyright(c)Kishigawa Minami</div>
		</div>
	</body>
</html>