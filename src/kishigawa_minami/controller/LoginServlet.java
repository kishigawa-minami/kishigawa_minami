package kishigawa_minami.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kishigawa_minami.beans.User;
import kishigawa_minami.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");

        LoginService loginService = new LoginService();
        User user = loginService.login(login_id, password);

        HttpSession session = request.getSession();
        List<String> messages = new ArrayList<String>();
        if (user == null)  {

            messages.add("ログインに失敗しました。");
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("login");
            return;
        }

        if(user.getStop() == 1) {
        	messages.add("停止中です");
        	session.setAttribute("errorMessages", messages);
        	response.sendRedirect("login");
        	return;
        }
        session.setAttribute("loginUser", user);
        response.sendRedirect("./");
    }
}