package kishigawa_minami.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kishigawa_minami.beans.User;
import kishigawa_minami.service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<User> userList = new UserService().all();

		session.setAttribute("userList",userList);

		User user = (User) session.getAttribute("loginUser");

		List<String> messages = new ArrayList<String>();
		if(user == null) {
			messages.add("ログインしてください");
			session.setAttribute("errorMessages" , messages);
			response.sendRedirect("login");
			return;
		} else if (user.getPosition() != 1) {
			messages.add("権限がありません");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
			return;
		}

		request.getRequestDispatcher("management.jsp").forward(request, response);
	}
}