package kishigawa_minami.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kishigawa_minami.beans.Comment;
import kishigawa_minami.beans.Message;
import kishigawa_minami.beans.UserComment;
import kishigawa_minami.beans.UserMessage;
import kishigawa_minami.service.CommentService;
import kishigawa_minami.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		UserMessage userMessage = new UserMessage();

		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		//全件出力
		if((request.getParameter("category") == null || request.getParameter("category") == "")
				&& (request.getParameter("start") == null || request.getParameter("start") == "")
				&& (request.getParameter("end") == null || request.getParameter("end") == ""))
		{
		List<UserMessage> messages = new MessageService().getMessage();
		request.setAttribute("messages", messages);

		List<UserComment> comments = new CommentService().getComment();
		request.setAttribute("comments", comments);

		request.getRequestDispatcher("/top.jsp").forward(request, response);
		return;
		}

		//片方しか日付指定しなかった時にもう片方に代入する分岐処理
		String startDate = request.getParameter("start");
		if(startDate == "") {
			startDate = "2019-01-01";
		}
		Timestamp start = null;
		try {
			start = new Timestamp(
					new SimpleDateFormat("yyyy-MM-dd").parse(startDate).getTime());
		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		String endDate = request.getParameter("end");
		if(endDate == "") {
			endDate = "3000-01-01";
		}
		Timestamp end = null;
		try {
			end = new Timestamp(
					new SimpleDateFormat("yyyy-MM-dd").parse(endDate).getTime());
		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		userMessage.setCategory(request.getParameter("category"));
		userMessage.setCreated_date(start);
		userMessage.setEndCreated_date(end);

		//複合条件で検索
		if((request.getParameter("category") != "")
				&& (request.getParameter("start") != "" || request.getParameter("end") != "")) {

			List<UserMessage> categoryPeriod = new MessageService().getCategoryPeriod(userMessage);
			request.setAttribute("messages", categoryPeriod);

			List<UserComment> comments = new CommentService().getComment();
			request.setAttribute("comments", comments);

			request.getRequestDispatcher("/top.jsp").forward(request, response);
			return;
		}

		//カテゴリー指定でのみ検索
		if((request.getParameter("category") != "") &&(request.getParameter("start") == "")
				&&(request.getParameter("end") == "")) {
			userMessage.setCategory(request.getParameter("category"));
			List<UserMessage> category = new MessageService().getCategory(userMessage);
			request.setAttribute("messages", category);

			List<UserComment> comments = new CommentService().getComment();
			request.setAttribute("comments", comments);

			request.getRequestDispatcher("/top.jsp").forward(request, response);
			return;
		}

		//期間指定でのみ検索
		if((request.getParameter("start") != "" || request.getParameter("end") != "")
				&& (request.getParameter("category") == "")) {

			List<UserMessage> period = new MessageService().getPeriod(userMessage);
			request.setAttribute("messages", period);

			List<UserComment> comments = new CommentService().getComment();
			request.setAttribute("comments", comments);

			request.getRequestDispatcher("/top.jsp").forward(request, response);
			return;
		}

	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		if(request.getParameter("message.id") != null) {
			Message message = new Message();
			message.setId(Integer.parseInt(request.getParameter("message.id")));
			new MessageService().delete(message);
		}

		if(request.getParameter("comment.id") != null) {
			Comment comment = new Comment();
			comment.setId(Integer.parseInt(request.getParameter("comment.id")));
			new CommentService().delete(comment);
		}

		response.sendRedirect("./");
}
}