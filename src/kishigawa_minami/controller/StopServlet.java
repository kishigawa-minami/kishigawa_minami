package kishigawa_minami.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kishigawa_minami.beans.User;
import kishigawa_minami.service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		User stop = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
		byte stopNumber = stop.getStop();

			if(stop.getStop() == 0) {
				stopNumber ++;
				stop.setStop(stopNumber);
			} else {
				stopNumber --;
				stop.setStop(stopNumber);
			}
			new UserService().stop(stop);

		response.sendRedirect("management");
	}
}