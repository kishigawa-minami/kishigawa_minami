package kishigawa_minami.dao;

import static kishigawa_minami.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import kishigawa_minami.beans.UserComment;
import kishigawa_minami.exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> getUserComments(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");		//内部結合
			sql.append("comments.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("comments.post_id as post_id, ");
			sql.append("users.name as name, ");
			sql.append("comments.text as text, ");
			sql.append("comments.created_date as created_date ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.login_id = users.id ");
			sql.append("INNER JOIN messages ");
			sql.append("ON comments.post_id = messages.id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toUserCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//全件取得用メソッド
	private List<UserComment> toUserCommentList(ResultSet rs)
			throws SQLException {

		//メソッドの結果として返すリスト
		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			//結果を表示する
			while (rs.next()) {
				//1件ずつオブジェクトを生成して結果を詰める
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				int post_id = rs.getInt("post_id");
				String name = rs.getString("name");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserComment comment = new UserComment();
				comment.setId(id);
				comment.setLogin_id(login_id);
				comment.setPost_id(post_id);
				comment.setName(name);
				comment.setText(text);
				comment.setCreated_date(createdDate);

				//リストに追加
				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}