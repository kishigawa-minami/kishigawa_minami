package kishigawa_minami.dao;

import static kishigawa_minami.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import kishigawa_minami.beans.Comment;
import kishigawa_minami.exception.SQLRuntimeException;

public class CommentDao {
	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");		//テーブルにデータ登録
			sql.append("login_id");
			sql.append(", post_id");
			sql.append(", text");
			sql.append(", created_date");
			sql.append(") VALUES (");
			sql.append("?"); // login_id
			sql.append(", ?"); //post_id
			sql.append(", ?"); // text
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getLogin_id());
			ps.setInt(2, comment.getPost_id());
			ps.setString(3, comment.getText());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM comments WHERE id = ? ";

			ps = connection.prepareStatement(sql);

			ps.setInt(1, comment.getId());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}