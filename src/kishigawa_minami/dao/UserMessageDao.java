package kishigawa_minami.dao;

import static kishigawa_minami.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import kishigawa_minami.beans.UserMessage;
import kishigawa_minami.exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");		//内部結合
			sql.append("messages.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("messages.title as title, ");
			sql.append("messages.category as category, ");
			sql.append("messages.text as text, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.login_id = users.id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				String title = rs.getString("title");
				String category = rs.getString("category");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserMessage message = new UserMessage();
				message.setId(id);
				message.setLogin_id(login_id);
				message.setName(name);
				message.setTitle(title);
				message.setCategory(category);
				message.setText(text);
				message.setCreated_date(createdDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<UserMessage> selectCategory(Connection connection,UserMessage userMessage){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("messages.title as title, ");
			sql.append("messages.category as category, ");
			sql.append("messages.text as text, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.login_id = users.id ");
			sql.append("WHERE category like ? ");
			sql.append("ORDER BY created_date DESC");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, "%" + userMessage.getCategory() + "%");

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			System.out.println(ret);
			return ret;
		}catch (SQLException e){
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	public List<UserMessage> selectPeriod(Connection connection,UserMessage userMessage){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("messages.title as title, ");
			sql.append("messages.category as category, ");
			sql.append("messages.text as text, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.login_id = users.id ");
			sql.append("WHERE messages.created_date ");
			sql.append("BETWEEN ? AND DATE_ADD( ? ,INTERVAL 1 DAY)");
			sql.append("ORDER BY created_date DESC");

			ps = connection.prepareStatement(sql.toString());
			ps.setTimestamp(1, userMessage.getCreated_date());
			ps.setTimestamp(2, userMessage.getEndCreated_date());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		}catch (SQLException e){
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<UserMessage> selectCategoryPeriod(Connection connection,UserMessage userMessage){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("messages.title as title, ");
			sql.append("messages.category as category, ");
			sql.append("messages.text as text, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.login_id = users.id ");
			sql.append("WHERE messages.category like ? ");
			sql.append("AND messages.created_date BETWEEN ? AND ? ");
			sql.append("ORDER BY created_date DESC");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, "%" + userMessage.getCategory() + "%");
			ps.setTimestamp(2, userMessage.getCreated_date());
			ps.setTimestamp(3, userMessage.getEndCreated_date());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			System.out.println(ret);

			return ret;
		}catch (SQLException e){
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}