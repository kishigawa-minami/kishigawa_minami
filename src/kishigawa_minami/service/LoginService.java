package kishigawa_minami.service;

import static kishigawa_minami.utils.CloseableUtil.*;
import static kishigawa_minami.utils.DBUtil.*;

import java.sql.Connection;

import kishigawa_minami.beans.User;
import kishigawa_minami.dao.UserDao;
import kishigawa_minami.utils.CipherUtil;

public class LoginService {

	public User login(String login_id, String password) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection, login_id, encPassword);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}